export ZSH="$HOME/.oh-my-zsh"

ZSH_THEME="half-life"
HYPHEN_INSENSITIVE="true"

zstyle ':omz:update' mode auto
zstyle ':omz:update' frequency 7

ENABLE_CORRECTION="false"
COMPLETION_WAITING_DOTS="true"
HIST_STAMPS="dd/mm/yyyy"
plugins=(git thefuck cp rsync ssh-agent sublime gas docker gitignore github vagrant npm nvm bundler gem rbenv ruby pip python pyenv virtualenv dnf systemd battery emoji-clock emoji themes wakatime)

source $ZSH/oh-my-zsh.sh
source ~/.zsh_aliases

# Ruby
alias rbenv="/home/leo/.rbenv/libexec/rbenv"
export PATH="$HOME/.rbenv/bin:$HOME/.rbenv/plugins/ruby-build/bin:$PATH"
eval "$(rbenv init -)"

# Environment Modules
module use "$HOME/.modulefiles"
module load micro btop


fpath+=${ZDOTDIR:-~}/.zsh_functions
neofetch --title_fqdn on --package_managers on --os_arch on --cpu_speed on --cpu_temp C --cpu_cores physical --distro_shorthand on --kernel_shorthand on --uptime_shorthand on --refresh_rate on --gpu_brand on --gpu_type dedicated --de_version off --gtk2 off --gtk3 off --shell_path on --disk_show '/' --disk_subtitle dir --disk_percent on --memory_percent on --memory_unit gib --bold on --color_blocks on --bar_char █ ░ --cpu_display infobar --backend caca --source ascii --size 20%
df / /home /storage